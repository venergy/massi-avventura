# Massi Avventura

## Version 1.2

A game with Sokoban qualities built with HTML/CSS/JS. 

Check out the working version [here](https://kineticvenergy.com/massi-avventura/index.html).

## Objective

Your objective is to get Massimo to the green exit. There are 40 levels to complete. As the levels progress you will encounter new objects and game mechanics.

Trial, error and exploration will get you through.

## Controls

Use your arrow keys to move Massimo around. Or swipe on mobile.

If you get stuck, press X to restart the level. Press Z to undo 1 move (up to 10 undos). Level progress will be saved in your browser (as a cookie). Grid progress will not be saved.

## Builder

Use the builder to create and share your own levels. Select an element either side of the grid and click a grid square to place that element.

Use 'Build URL' to get a link to your current build layout. Use this URL to continue working on your level later. 

Use 'Test Level' to play your level. Make sure the level can be completed.

Complete your test level and a 'replay' link will appear at the bottom of the grid. Click replay to view your replay. Share your replay URL to show others the solution.

## URLs and URL parameters

The build & test URLs contain the parameter of 'ld' which represents the 'level data'.

The replay URL contains both the 'ld' parameter and also the 'sd' parameter which represents the 'solution data'.

## Installation

This package should work straight out of the box. 

Network connection is required to load fonts and midi player for end-game tune.

## Levels and level data

Levels are stored in /levels/levelData.js as a set of nested arrays. Each array contains the level name (String) and a secondary array of 400 numbers (which represent the 400 game squares (top left to bottom right)). Each number represents a grid element. The list of elements can be found in /js/gridElements.js 

The builder will output an array of 400 numbers in the text area below the build area whenever the user presses 'Output Data', 'Test Level' or 'Build URL'. You can add to the levels by adding a new array to the levelData array.

## Licenses

### Massi

You can find the license for Massi in /License.txt

You may use this code in personal and commercial projects.

Credit (kineticVenergy or kineticvenergy.com) would be nice but is not mandatory.

### Assets

Graphics: /images/License.txt

Sound: /sounds/License.txt

## FAQs

### Are all the levels solvable?
I can confirm all levels supplied in this codebase are solvable. 